FROM ubuntu:18.04
RUN apt-get update && apt-get install -y gcc zlib1g-dev
ADD graalvm-ce-1.0.0-rc10-linux-amd64.tar.gz /
ENV PATH /graalvm-ce-1.0.0-rc10/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
WORKDIR /graalvm-demo
COPY . /graalvm-demo
RUN ./gradlew -v
RUN ./gradlew nativeImage

FROM alpine
WORKDIR /graalvm-demo
COPY --from=0 /graalvm-demo/app .
EXPOSE 8080
CMD ./app