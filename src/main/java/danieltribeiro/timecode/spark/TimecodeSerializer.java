package danieltribeiro.timecode.spark;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import danieltribeiro.timecode.Timecode;

public class TimecodeSerializer implements JsonSerializer<Timecode> {
  @Override
  public JsonElement serialize(Timecode src, java.lang.reflect.Type typeOfSrc, JsonSerializationContext context) {
    JsonObject json = new JsonObject();

    json.addProperty("string", src.toString());
    json.addProperty("milliseconds", src.getTotalMilis());
    json.addProperty("frames", src.getTotalFrames());
    json.addProperty("frameRate", src.getFramerate().getName());

    return json;
  }
}